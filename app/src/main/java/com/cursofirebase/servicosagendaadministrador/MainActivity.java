package com.cursofirebase.servicosagendaadministrador;

import androidx.annotation.NonNull;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.Manifest;
import android.os.Bundle;
import android.view.MenuItem;

import com.cursofirebase.servicosagendaadministrador.fragment.FragmentCalendario;
import com.cursofirebase.servicosagendaadministrador.fragment.FragmentHome;
import com.cursofirebase.servicosagendaadministrador.util.Permissao;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    private BottomNavigationView bottomNavigationView;
    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener;


    private FragmentHome fragmentHome;
    private FragmentCalendario fragmentCalendario;

    private Fragment fragment;
    private FragmentManager fragmentManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        bottomNavigationView = (BottomNavigationView)findViewById(R.id.bottomNavigationViewAdmin);


        navigationBottom();


        fragmentHome = new FragmentHome();
        fragmentCalendario = new FragmentCalendario();


        fragmentManager = getSupportFragmentManager();

        fragmentManager.beginTransaction().replace(R.id.frameLayout_FragmentAdmin,fragmentHome).commit();



        permissao();

    }


    private void permissao(){


        String permissoes[] = new String[]{

                Manifest.permission.GET_ACCOUNTS, Manifest.permission.READ_CONTACTS
        };


        Permissao.validate(this,333,permissoes);


    }




    private void navigationBottom(){



        onNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {


                switch (menuItem.getItemId()){


                    case R.id.item_navegacao_home:


                        fragment = fragmentHome;



                        break;

                    case R.id.item_navegacao_calendario:



                        fragment = fragmentCalendario;

                        break;
                }



                getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout_FragmentAdmin,fragment).commit();
////
                return true;
            }
        };



        bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);


    }




    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
