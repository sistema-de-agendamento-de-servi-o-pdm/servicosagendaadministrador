package com.cursofirebase.servicosagendaadministrador.fragment;


import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CalendarView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;


import com.cursofirebase.servicosagendaadministrador.R;
import com.cursofirebase.servicosagendaadministrador.activity.HorariosActivity;
import com.cursofirebase.servicosagendaadministrador.util.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentCalendario extends Fragment implements CalendarView.OnDateChangeListener {



    private CalendarView calendarView;

    private int dia_Atual;
    private int mes_Atual;
    private int ano_Atual;



    public FragmentCalendario() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_calendario, container, false);


        calendarView = (CalendarView) view.findViewById(R.id.calendarView_Calendario);




        obterDataAtual();


        calendarView.setOnDateChangeListener(this);


        versaoLollipop();


        return view;
    }






    //---------------------------------------- OBTER DATA ATUAL ----------------------------------------



    private void obterDataAtual(){

        long dataLong = calendarView.getDate(); //123456465465213

        Locale locale = new Locale("pt","BR");

        SimpleDateFormat dia = new SimpleDateFormat("dd",locale);
        SimpleDateFormat mes = new SimpleDateFormat("MM",locale);
        SimpleDateFormat ano = new SimpleDateFormat("yyyy",locale);




        dia_Atual = Integer.parseInt(dia.format(dataLong));

        mes_Atual = Integer.parseInt(mes.format(dataLong));

        ano_Atual = Integer.parseInt(ano.format(dataLong));



     //   Toast.makeText(getContext(),
         //       "Dia: "+dia_Atual+ "\nMes: "+ mes_Atual + "\nAno: "+ano_Atual,Toast.LENGTH_LONG).show();

    }












    //---------------------------------------- CLICK CALENDARIO ----------------------------------------

    @Override
    public void onSelectedDayChange(CalendarView calendarView,
                                    int anoSelecionado, int mesSelecionado, int diaSelecionado) {


        // Janeiro = 0           1
        // Fevereiro = 1        2
        // Março = 2            3

        //Junho = 5         6
        //Julho = 6

        int mes = mesSelecionado + 1;

    //    Toast.makeText(getContext(),
      //          "Dia: "+diaSelecionado+ "\nMes: "+ mes + "\nAno: "+anoSelecionado,Toast.LENGTH_LONG).show();



        dataSelecionada(diaSelecionado,mes,anoSelecionado);

    }






    private void dataSelecionada(int diaSelecionado,int mesSelecionado,int anoSelecionado){



        Locale locale = new Locale("pt","BR");

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy",locale);

        Calendar data = Calendar.getInstance();


        try {
            data.setTime(simpleDateFormat.parse(diaSelecionado+"/"+mesSelecionado+"/"+anoSelecionado));




                if (Util.statusInternet_MoWi(getContext())){

                    String dia = String.valueOf(diaSelecionado);
                    String mes = String.valueOf(mesSelecionado);
                    String ano = String.valueOf(anoSelecionado);

                    ArrayList<String> dataList = new ArrayList<String>();

                    dataList.add(dia);// posicao 0
                    dataList.add(mes);// posicao 1
                    dataList.add(ano);// posicao 2

                    Intent intent = new Intent(getContext(), HorariosActivity.class);

                    intent.putExtra("data",dataList);

                    startActivity(intent);
                    // CHAMAR A NOSSA PROXIMA ACTIVITY

                }else{

                    Toast.makeText(getContext(),"Erro - Sem conexão com a internet",Toast.LENGTH_LONG).show();
                }



        } catch (ParseException e) {
            e.printStackTrace();
        }


    }









    //---------------------------------------- VERSOES ANTIGAS ANDROID ----------------------------------------


    private void versaoLollipop(){



        int versao = Build.VERSION.SDK_INT;


        if( versao <= Build.VERSION_CODES.LOLLIPOP){


            WindowManager windowManager = (WindowManager)getActivity().getSystemService(getActivity().WINDOW_SERVICE);
            Display display = windowManager.getDefaultDisplay();


            Point tamanho = new Point();
            display.getSize(tamanho);


            int width = tamanho.x;

            if (width == 480){


                calendarView.getLayoutParams().width = 730;
                calendarView.getLayoutParams().height = 500;


            }else{


                calendarView.getLayoutParams().width = 800;
                calendarView.getLayoutParams().height = 650;

            }

        }else{
            // versoes mais novas do android. Não precisa configurar manualmente o calendario.

        }
    }



}
