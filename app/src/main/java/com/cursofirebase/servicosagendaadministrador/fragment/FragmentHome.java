package com.cursofirebase.servicosagendaadministrador.fragment;


import android.app.Activity;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.service.media.MediaBrowserService;
import android.telephony.PhoneNumberUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.cursofirebase.servicosagendaadministrador.R;
import com.cursofirebase.servicosagendaadministrador.modelo.Empresa;
import com.cursofirebase.servicosagendaadministrador.util.DialogProgress;
import com.cursofirebase.servicosagendaadministrador.util.Util;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.transform.Result;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentHome extends Fragment implements View.OnClickListener {


    private ImageView imageView;
    private ProgressBar progressBar;

    private EditText editText_Informacao;
    private EditText editText_ValorServico;
    private EditText editText_NumeroContato;
    private CardView cardView_AlterarDados;




    //firebase
    private FirebaseDatabase database;
    private DatabaseReference reference;
    private ValueEventListener valueEventListener;

    private static boolean offlineFirebase = false;


    private Empresa empresaDados = new Empresa();
    private Uri uri = null;


    public FragmentHome() {
        // Required empty public constructor
    }




    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        database = FirebaseDatabase.getInstance();

        iniciarModoOfflineFirebase();

        reference = database.getReference().child("BD").child("Home").child("Dados");
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_home, container, false);


        imageView = (ImageView) view.findViewById(R.id.imageView_Home);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar_Home);

        editText_Informacao = (EditText)view.findViewById(R.id.editText_Home_Informacao);
        editText_NumeroContato = (EditText)view.findViewById(R.id.editText_Home_NumeroContato);
        editText_ValorServico = (EditText)view.findViewById(R.id.editText_Home_ValorServico);

        cardView_AlterarDados = (CardView) view.findViewById(R.id.cardView_Home_AlterarDados);


        imageView.setOnClickListener(this);
        cardView_AlterarDados.setOnClickListener(this);


        return view;
    }


    //--------------------------- MODO OFFLINE FIREBASE-------------------------------------


    private void iniciarModoOfflineFirebase(){

        try{

            if (!offlineFirebase){

                FirebaseDatabase.getInstance().setPersistenceEnabled(true);
                offlineFirebase = true;

            }else{

                //já foi ativado offline
            }

        }catch (Exception e){

        }

    }


    //--------------------------- ACAO DE CLICK CARDVIEW -------------------------------------


    @Override
    public void onClick(View view) {


        switch (view.getId()){


            case R.id.imageView_Home:


                obterImagem_Galeria();

                break;


            case R.id.cardView_Home_AlterarDados:

                validarDadosAlterados();

                break;




        }



    }



    //------------------------------OBTER IMAGENS GALERIA-----------------------------------------------------




    private void obterImagem_Galeria(){


        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);


        startActivityForResult(Intent.createChooser(intent,"Escolhe uma imagem"),10);


    }







    //------------------------------ALTERAR DADOS----------------------------------------------------



    private void validarDadosAlterados(){


        String informacao = editText_Informacao.getText().toString();
        String valorServico = editText_ValorServico.getText().toString();
        String numeroContato = editText_NumeroContato.getText().toString();


     if(Util.statusInternet_MoWi(getContext())){


         if(!informacao.isEmpty() && !valorServico.isEmpty() && !numeroContato.isEmpty()){


             if (empresaDados.getInformacao().equals(informacao) &&
                     empresaDados.getValorServico().equals(valorServico) &&
                     empresaDados.getNumeroContato().equals(numeroContato) && uri == null){

                 Toast.makeText(getContext(),"Você não alterou nenhuma informaçao",Toast.LENGTH_LONG).show();

             }else{

                 String imagemUrl = empresaDados.getImagemUrl();
                 String latitude = empresaDados.getLatitude();
                 String longitude = empresaDados.getLongitude();

                 if (uri != null){


                     alterarImagemStorage(informacao,valorServico,numeroContato,latitude,longitude);

                 }else{

                     alterarDadosDatabase(informacao,valorServico,numeroContato,imagemUrl,latitude,longitude);

                 }

             }

         }else{

             Toast.makeText(getContext(),"Preencha todos os campos obrigatórios",Toast.LENGTH_LONG).show();

         }


     }else{

         Toast.makeText(getContext(),"ERRO - Verifique se você possui alguma conexão com a Internet.",Toast.LENGTH_LONG).show();

     }


    }










    //------------------------------ALTERAR IMAGEM NO STORAGE CASO PRECISE----------------------------------------------------



    private void alterarImagemStorage(final String informacao, final String valorServico,
                                      final String numeroContato, final String latitude, final String longitude){


        final DialogProgress dialogProgress = new DialogProgress();
        dialogProgress.show(getChildFragmentManager(),"");


        FirebaseStorage storage = FirebaseStorage.getInstance();

        StorageReference storageReference = storage.getReference().child("imagemlogo");

        final StorageReference nomeImagem =storageReference.child("LogoEmpresa"+System.currentTimeMillis()+".jpg");



        UploadTask uploadTask = nomeImagem.putFile(uri);


        uploadTask.addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {


                if(task.isSuccessful()){

                    dialogProgress.dismiss();


                    nomeImagem.getDownloadUrl().addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {

                            if( task.isSuccessful()){


                                String urlImagem = task.getResult().toString();

                                alterarDadosDatabase(informacao, valorServico, numeroContato,
                                        urlImagem, latitude, longitude);

                            }
                        }
                    });


                }else{

                    dialogProgress.dismiss();
                    Toast.makeText(getContext(),"Falha no Upload",Toast.LENGTH_LONG).show();

                }
            }
        });
    }







    //------------------------------ALTERAR DADOS DATABASE----------------------------------------------------


    private void alterarDadosDatabase(String informacao, String valorServico, String numeroContato,
                                      String imagemUrl, String latitude, String longitude){



        Empresa empresa = new Empresa(imagemUrl,informacao,latitude,longitude,numeroContato,valorServico);

        final DialogProgress dialogProgress = new DialogProgress();
        dialogProgress.show(getChildFragmentManager(),"");


        FirebaseDatabase database = FirebaseDatabase.getInstance();

        DatabaseReference databaseReference = database.getReference().child("BD").child("Home");



        Map<String, Object> atualizacao = new HashMap<>();

        atualizacao.put("Dados",empresa);


        databaseReference.updateChildren(atualizacao).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {


                if(task.isSuccessful()){

                    dialogProgress.dismiss();
                    Toast.makeText(getContext(),"Sucesso ao Realizar Alteração",Toast.LENGTH_LONG).show();


                }else{

                    dialogProgress.dismiss();
                    Toast.makeText(getContext(),"Erro ao Realizar Alteração",Toast.LENGTH_LONG).show();

                }

            }
        });

    }




    //------------------------------OUVINTE FIREBASE-----------------------------------------------------


    private void ouvinte(){


        if (valueEventListener == null){



            valueEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                    if(dataSnapshot.exists()){


                        Empresa empresa  = dataSnapshot.getValue(Empresa.class);


                        empresaDados = empresa;

                        if(!empresa.getInformacao().isEmpty() && !empresa.getValorServico().isEmpty()){

                            atualizarDados(empresa);
                        }

                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            };


            reference.addValueEventListener(valueEventListener);

        }



    }




    private void atualizarDados(Empresa empresa){



        editText_Informacao.setText(empresa.getInformacao());
        editText_ValorServico.setText(empresa.getValorServico());
        editText_NumeroContato.setText(empresa.getNumeroContato());





                Glide.with(getContext()).asBitmap().load(empresa.getImagemUrl()).listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {

                        Toast.makeText(getContext(),"Erro ao realizar Donwload de imagem",Toast.LENGTH_LONG).show();
                        progressBar.setVisibility(View.GONE);

                        return false;
                    }


            @Override
            public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {


                progressBar.setVisibility(View.GONE);


                return false;
            }
        }).into(imageView);



    }







    //------------------------------ONACTIVITY RESULT-----------------------------------------------------


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if ( resultCode == Activity.RESULT_OK){


            if (requestCode == 10){


                if(data != null){

                    Uri uriImagem = data.getData();

                    uri = uriImagem;


                    Glide.with(getContext()).asBitmap().load(uriImagem).listener(new RequestListener<Bitmap>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                            // erro ao carregar imagem

                            Toast.makeText(getContext(),"Erro ao selecionar imagem",Toast.LENGTH_LONG).show();


                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {

                            // ok

                            return false;
                        }
                    }).into(imageView);

                }
            }

        }

    }












    //------------------------------CICLO DE VIDA FRAGMENT-----------------------------------------------------

    @Override
    public void onStart() {
        super.onStart();


        ouvinte();

    }


    @Override
    public void onDestroy() {
        super.onDestroy();


        if (valueEventListener != null){


            reference.removeEventListener(valueEventListener);

            valueEventListener = null;

            //valueEventListener!=null

        }


    }
}
